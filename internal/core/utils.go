package core

import (
	"net"
	"strconv"
	"strings"

	"github.com/go-sql-driver/mysql"
	"gitlab.com/evolves-fr/gommon"
)

func MysqlDSN(dsn gommon.DSN) string {
	params := make(map[string]string)
	for key, values := range dsn.Queries {
		if len(values) > 0 {
			params[key] = values[0]
		}
	}

	config := mysql.NewConfig()
	config.User = dsn.Username
	config.Passwd = dsn.Password
	config.Net = gommon.Default(dsn.Protocol, "tcp")
	config.Addr = net.JoinHostPort(dsn.Endpoint, strconv.Itoa(dsn.Port))
	config.DBName = dsn.Path
	config.Params = params

	return config.FormatDSN()
}

func PostgresDSN(dsn gommon.DSN) string {
	config := make([]string, 0)

	if !gommon.Empty(dsn.Endpoint) {
		config = append(config, "host="+dsn.Endpoint)
	}

	if !gommon.Empty(dsn.Port) {
		config = append(config, "port="+strconv.Itoa(dsn.Port))
	}

	if !gommon.Empty(dsn.Path) {
		config = append(config, "dbname="+dsn.Path)
	}

	if !gommon.Empty(dsn.Username) {
		config = append(config, "user="+dsn.Username)
	}

	if !gommon.Empty(dsn.Password) {
		config = append(config, "password="+dsn.Password)
	}

	for key, values := range dsn.Queries {
		if !gommon.Empty(key) && len(values) > 0 {
			config = append(config, key+"="+values[0])
		}
	}

	return strings.Join(config, " ")
}
