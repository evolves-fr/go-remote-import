package core

import (
	"html/template"
	"io"
	"io/fs"

	"github.com/labstack/echo/v4"
)

func Renderer(src fs.FS) echo.Renderer {
	return renderer{fs: src}
}

type renderer struct {
	fs fs.FS
	*template.Template
}

func (r renderer) Functions() template.FuncMap {
	return map[string]any{
		"version": func() string { return Version },
	}
}

func (r renderer) Render(w io.Writer, name string, data interface{}, _ echo.Context) error {
	file, err := fs.ReadFile(r.fs, name)
	if err != nil {
		return err
	}

	tpl, err := template.New(name).Funcs(r.Functions()).Parse(string(file))
	if err != nil {
		return err
	}

	return tpl.ExecuteTemplate(w, name, data)
}
