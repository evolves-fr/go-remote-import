package core

type Config struct {
	Debug       bool
	Environment string
	Secret      string
	Database    string
}
