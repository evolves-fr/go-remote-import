package core

import (
	"errors"
	"gitlab.com/evolves-fr/gommon"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Core interface {
	Config() Config
	DB() *gorm.DB
}

func New(cfg Config) (Core, error) {
	dsn, err := gommon.ParseDSN(cfg.Database)
	if err != nil {
		return nil, err
	}

	var dialector gorm.Dialector
	switch dsn.Scheme {
	case "mysql":
		dialector = mysql.Open(MysqlDSN(dsn))
	case "psql", "postgres":
		dialector = postgres.Open(PostgresDSN(dsn))
	default:
		return nil, errors.New("unknown database type")
	}

	db, err := gorm.Open(dialector, &gorm.Config{})
	if err != nil {
		return nil, err
	}

	if cfg.Debug {
		db = db.Debug()
	}

	return &core{config: cfg, db: db}, nil
}

type core struct {
	config Config
	db     *gorm.DB
}

func (c core) Config() Config { return c.config }
func (c core) DB() *gorm.DB   { return c.db }
