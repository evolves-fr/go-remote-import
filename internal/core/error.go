package core

import (
	"fmt"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

func ErrorHandler() echo.HTTPErrorHandler {
	return func(e error, c echo.Context) {
		fmt.Printf("%T = %+v\n", e, e)

		err := echo.ErrInternalServerError

		switch v := e.(type) {
		case *echo.HTTPError:
			err = v
		case validator.ValidationErrors:
			err = echo.NewHTTPError(http.StatusBadRequest, v.Error())
		default:
			switch e {
			case gorm.ErrRecordNotFound:
				err = echo.ErrNotFound
			}
		}

		_ = c.Render(err.Code, "error.gohtml", err)
	}
}
