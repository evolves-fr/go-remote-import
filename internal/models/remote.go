package models

import (
	"fmt"

	"gitlab.com/evolves-fr/gommon"
)

type Remote struct {
	Name        string `gorm:"column:name;primaryKey" validate:"required" form:"name"`
	Description string `gorm:"column:description;" validate:"-" form:"description"`
	VCS         string `gorm:"column:vcs;" validate:"required,oneof=bzr fossil git hg svn mod" form:"vcs"`
	Repository  string `gorm:"column:repository;" validate:"required" form:"repository"`
	Home        string `gorm:"column:home;" validate:"-" form:"home"`
	Directory   string `gorm:"column:directory;" validate:"-" form:"directory"`
	File        string `gorm:"column:file;" validate:"-" form:"file"`
}

func (r Remote) GoImport() string {
	return fmt.Sprintf("%s %s %s", r.Name, r.VCS, r.Repository)
}

func (r Remote) GoSource() string {
	return fmt.Sprintf("%s %s %s %s", r.Name,
		gommon.Default(r.Home, "_"),
		gommon.Default(r.Directory, "_"),
		gommon.Default(r.File, "_"),
	)
}
