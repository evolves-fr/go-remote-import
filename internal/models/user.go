package models

import (
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	Login    string `gorm:"column:login;primaryKey" validate:"required" form:"login"`
	Password string `gorm:"column:password;" validate:"required" form:"password"`
}

func (User) TableName() string {
	return "users"
}

func (u *User) BeforeSave(_ *gorm.DB) error {
	if cost, err := bcrypt.Cost([]byte(u.Password)); (cost == 0 || err != nil) && u.Password != "" {
		hash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
		if err != nil {
			return err
		}

		u.Password = string(hash)
	}

	return nil
}
