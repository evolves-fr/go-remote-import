package middlewares

import "github.com/labstack/echo/v4"

func URL() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			if ctx.Request().URL.Scheme == "" {
				ctx.Request().URL.Scheme = "http"

				if ctx.Request().TLS != nil {
					ctx.Request().URL.Scheme += "s"
				}
			}

			if ctx.Request().URL.Host == "" {
				ctx.Request().URL.Host = ctx.Request().Host
			}

			return next(ctx)
		}
	}
}
