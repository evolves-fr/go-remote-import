package middlewares

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/evolves-fr/go-remote-import/internal/core"
)

func Context(c core.Core) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			return next(&core.Context{Context: ctx, Core: c})
		}
	}
}
