package middlewares

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

func Authentication(secret string) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			cookie, err := ctx.Cookie("token")
			if err != nil {
				return ctx.Redirect(http.StatusFound, "/admin/sign-in/")
			}

			token, err := jwt.ParseWithClaims(cookie.Value, &jwt.StandardClaims{}, func(token *jwt.Token) (any, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
				}

				return []byte(secret), nil
			})
			if err != nil {
				return echo.ErrUnauthorized.SetInternal(err)
			}

			if !token.Valid {
				return echo.ErrUnauthorized.SetInternal(errors.New("token not valid"))
			}

			fmt.Printf("%T\n", token.Claims)
			fmt.Printf("%#v\n", token.Claims)

			claims, ok := token.Claims.(*jwt.StandardClaims)
			if !ok {
				return echo.ErrUnauthorized.SetInternal(errors.New("invalid claims"))
			}

			ctx.Set("login", claims.Id)

			return next(ctx)
		}
	}
}
