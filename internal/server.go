package internal

import (
	"embed"
	"io/fs"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/evolves-fr/go-remote-import/internal/controllers"
	"gitlab.com/evolves-fr/go-remote-import/internal/core"
	"gitlab.com/evolves-fr/go-remote-import/internal/middlewares"
	"gitlab.com/evolves-fr/go-remote-import/internal/models"
)

var (
	//go:embed templates
	templatesFS embed.FS

	//go:embed public
	publicFS embed.FS
)

func NewServer(cfg core.Config, address string) error {
	templates, err := fs.Sub(templatesFS, "templates")
	if err != nil {
		return err
	}

	public, err := fs.Sub(publicFS, "public")
	if err != nil {
		return err
	}

	c, err := core.New(cfg)
	if err != nil {
		return err
	}

	if err = c.DB().AutoMigrate(&models.Remote{}); err != nil {
		panic(err)
	}

	if err = c.DB().AutoMigrate(&models.User{}); err != nil {
		panic(err)
	}

	r := echo.New()

	r.HideBanner = true
	r.Renderer = core.Renderer(templates)
	r.HTTPErrorHandler = core.ErrorHandler()

	r.Use(middleware.Recover())
	r.Use(middleware.Logger())
	r.Use(middlewares.Context(c))
	r.Use(middlewares.URL())

	r.GET("/", controllers.Index)
	r.GET("/*", controllers.Remote)

	r.GET("/admin/sign-in/", controllers.SignIn)
	r.POST("/admin/sign-in/", controllers.Authenticate)
	r.GET("/admin/sign-out/", controllers.SignOut)

	admin := r.Group("/admin", middlewares.Authentication(c.Config().Secret))
	{
		admin.GET("/remotes/", controllers.ListRemotes)
		admin.GET("/remotes/new/", controllers.NewRemote)
		admin.POST("/remotes/new/", controllers.CreateRemote)
		admin.GET("/remotes/:name/", controllers.GetRemote)
		admin.GET("/remotes/:name/edit/", controllers.EditRemote)
		admin.POST("/remotes/:name/edit/", controllers.UpdateRemote)
		admin.GET("/remotes/:name/delete/", controllers.DeleteRemote)
		admin.POST("/remotes/:name/delete/", controllers.RemoveRemote)

		admin.GET("/users/", controllers.ListUsers)
		admin.GET("/users/new/", controllers.NewUser)
		admin.POST("/users/new/", controllers.CreateUser)
		admin.GET("/users/:login/", controllers.GetUser)
		admin.GET("/users/:login/edit/", controllers.EditUser)
		admin.POST("/users/:login/edit/", controllers.UpdateUser)
		admin.GET("/users/:login/delete/", controllers.DeleteUser)
		admin.POST("/users/:login/delete/", controllers.RemoveUser)
	}

	r.GET("/assets/*", func(c echo.Context) error {
		c.Response().Header().Set("Cache-Control", "public, max-age=604800")
		http.FileServer(http.FS(public)).ServeHTTP(c.Response(), c.Request())
		return nil
	})

	return r.Start(address)
}
