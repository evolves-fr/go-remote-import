package controllers

import (
	"net/http"
	"net/url"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/evolves-fr/go-remote-import/internal/core"
	"gitlab.com/evolves-fr/go-remote-import/internal/models"
	"golang.org/x/crypto/bcrypt"
)

// Admin - Authentication

func SignIn(ctx echo.Context) error {
	return ctx.Render(http.StatusOK, "admin/login.gohtml", nil)
}

func Authenticate(ctx echo.Context) error {
	c := ctx.(*core.Context)

	login := ctx.FormValue("login")
	password := ctx.FormValue("password")

	var user models.User

	if err := c.DB().First(&user, "`login`=?", login).Error; err != nil {
		return err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return echo.ErrUnauthorized
	}

	expiresAt := time.Now().UTC().Add(24 * time.Hour)

	claimsToken := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.StandardClaims{Id: user.Login, ExpiresAt: expiresAt.Unix()})

	token, err := claimsToken.SignedString([]byte(c.Core.Config().Secret))
	if err != nil {
		return err
	}

	ctx.SetCookie(&http.Cookie{
		Name:     "token",
		Value:    token,
		Path:     "/",
		Expires:  expiresAt,
		Secure:   ctx.IsTLS(),
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	})

	return ctx.Redirect(http.StatusFound, "/admin/remotes/")
}

func SignOut(ctx echo.Context) error {
	ctx.SetCookie(&http.Cookie{
		Name:     "token",
		Value:    "",
		Path:     "/",
		Expires:  time.Now().UTC().Add(-time.Hour),
		Secure:   ctx.IsTLS(),
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	})

	return ctx.Redirect(http.StatusFound, "/")
}

// Admin - Remotes

func ListRemotes(ctx echo.Context) error {
	c := ctx.(*core.Context)

	search := ctx.QueryParam("search")

	var remotes []models.Remote

	tx := c.DB().Model(&models.Remote{})

	if search != "" {
		tx = tx.Where("`name` LIKE ?", "%"+search+"%")
	}

	if err := tx.Find(&remotes).Error; err != nil {
		return err
	}

	return ctx.Render(http.StatusOK, "admin/remotes/index.gohtml", map[string]any{
		"Remotes": remotes,
		"Search":  search,
	})
}

func GetRemote(ctx echo.Context) error {
	c := ctx.(*core.Context)

	name, err := url.QueryUnescape(ctx.Param("name"))
	if err != nil {
		return err
	}

	var remote models.Remote

	if err = c.DB().First(&remote, "`name`=?", name).Error; err != nil {
		return err
	}

	return ctx.Render(http.StatusOK, "admin/remotes/show.gohtml", map[string]any{
		"Remote": remote,
	})
}

func NewRemote(ctx echo.Context) error {
	return ctx.Render(http.StatusOK, "admin/remotes/new.gohtml", nil)
}

func CreateRemote(ctx echo.Context) error {
	c := ctx.(*core.Context)

	var remote models.Remote

	if err := ctx.Bind(&remote); err != nil {
		return err
	}

	if err := validator.New().Struct(remote); err != nil {
		return err
	}

	if err := c.DB().Create(&remote).Error; err != nil {
		return err
	}

	return ctx.Redirect(http.StatusFound, "/admin/remotes/")
}

func EditRemote(ctx echo.Context) error {
	c := ctx.(*core.Context)

	name, err := url.QueryUnescape(ctx.Param("name"))
	if err != nil {
		return err
	}

	var remote models.Remote

	if err = c.DB().First(&remote, "`name`=?", name).Error; err != nil {
		return err
	}

	return ctx.Render(http.StatusOK, "admin/remotes/edit.gohtml", map[string]any{
		"Remote": remote,
	})
}

func UpdateRemote(ctx echo.Context) error {
	c := ctx.(*core.Context)

	name, err := url.QueryUnescape(ctx.Param("name"))
	if err != nil {
		return err
	}

	var remote models.Remote

	if err = c.DB().First(&remote, "`name`=?", name).Error; err != nil {
		return err
	}

	remoteName := remote.Name

	if err = ctx.Bind(&remote); err != nil {
		return err
	}

	if err = validator.New().Struct(remote); err != nil {
		return err
	}

	remote.Name = remoteName

	if err = c.DB().Save(&remote).Error; err != nil {
		return err
	}

	return ctx.Redirect(http.StatusFound, "/admin/remotes/")
}

func DeleteRemote(ctx echo.Context) error {
	c := ctx.(*core.Context)

	name, err := url.QueryUnescape(ctx.Param("name"))
	if err != nil {
		return err
	}

	var remote models.Remote

	if err = c.DB().First(&remote, "`name`=?", name).Error; err != nil {
		return err
	}

	return ctx.Render(http.StatusOK, "admin/remotes/delete.gohtml", map[string]any{
		"Remote": remote,
	})
}

func RemoveRemote(ctx echo.Context) error {
	c := ctx.(*core.Context)

	name, err := url.QueryUnescape(ctx.Param("name"))
	if err != nil {
		return err
	}

	var remote models.Remote

	if err = c.DB().First(&remote, "`name`=?", name).Error; err != nil {
		return err
	}

	if err = c.DB().Delete(&remote).Error; err != nil {
		return err
	}

	return ctx.Redirect(http.StatusFound, "/admin/remotes/")
}

// Admin - Users

func ListUsers(ctx echo.Context) error {
	c := ctx.(*core.Context)

	search := ctx.QueryParam("search")

	var users []models.User

	tx := c.DB().Model(&models.User{})

	if search != "" {
		tx = tx.Where("`login` LIKE ?", "%"+search+"%")
	}

	if err := tx.Find(&users).Error; err != nil {
		return err
	}

	return ctx.Render(http.StatusOK, "admin/users/index.gohtml", map[string]any{
		"Users":  users,
		"Search": search,
	})
}

func GetUser(ctx echo.Context) error {
	c := ctx.(*core.Context)

	login, err := url.QueryUnescape(ctx.Param("login"))
	if err != nil {
		return err
	}

	var user models.User

	if err = c.DB().First(&user, "`login`=?", login).Error; err != nil {
		return err
	}

	return ctx.Render(http.StatusOK, "admin/users/show.gohtml", map[string]any{
		"User": user,
	})
}

func NewUser(ctx echo.Context) error {
	return ctx.Render(http.StatusOK, "admin/users/new.gohtml", nil)
}

func CreateUser(ctx echo.Context) error {
	c := ctx.(*core.Context)

	var user models.User

	if err := ctx.Bind(&user); err != nil {
		return err
	}

	if err := validator.New().Struct(user); err != nil {
		return err
	}

	if err := c.DB().Create(&user).Error; err != nil {
		return err
	}

	return ctx.Redirect(http.StatusFound, "/admin/users/")
}

func EditUser(ctx echo.Context) error {
	c := ctx.(*core.Context)

	login, err := url.QueryUnescape(ctx.Param("login"))
	if err != nil {
		return err
	}

	var user models.User

	if err = c.DB().First(&user, "`login`=?", login).Error; err != nil {
		return err
	}

	return ctx.Render(http.StatusOK, "admin/users/edit.gohtml", map[string]any{
		"User": user,
	})
}

func UpdateUser(ctx echo.Context) error {
	c := ctx.(*core.Context)

	login, err := url.QueryUnescape(ctx.Param("login"))
	if err != nil {
		return err
	}

	var user models.User

	if err = c.DB().First(&user, "`login`=?", login).Error; err != nil {
		return err
	}

	userLogin := user.Login

	if err = ctx.Bind(&user); err != nil {
		return err
	}

	if err = validator.New().Struct(user); err != nil {
		return err
	}

	user.Login = userLogin

	if err = c.DB().Save(&user).Error; err != nil {
		return err
	}

	return ctx.Redirect(http.StatusFound, "/admin/users/")
}

func DeleteUser(ctx echo.Context) error {
	c := ctx.(*core.Context)

	login, err := url.QueryUnescape(ctx.Param("login"))
	if err != nil {
		return err
	}

	var user models.User

	if err = c.DB().First(&user, "`login`=?", login).Error; err != nil {
		return err
	}

	return ctx.Render(http.StatusOK, "admin/users/delete.gohtml", map[string]any{
		"User": user,
	})
}

func RemoveUser(ctx echo.Context) error {
	c := ctx.(*core.Context)

	login, err := url.QueryUnescape(ctx.Param("login"))
	if err != nil {
		return err
	}

	var user models.User

	if err = c.DB().First(&user, "`login`=?", login).Error; err != nil {
		return err
	}

	if err = c.DB().Delete(&user).Error; err != nil {
		return err
	}

	return ctx.Redirect(http.StatusFound, "/admin/users/")
}
