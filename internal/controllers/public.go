package controllers

import (
	"net/http"
	"path"

	"github.com/labstack/echo/v4"
	"gitlab.com/evolves-fr/go-remote-import/internal/core"
	"gitlab.com/evolves-fr/go-remote-import/internal/models"
)

func Index(ctx echo.Context) error {
	return ctx.Render(http.StatusOK, "index.gohtml", nil)
}

func Remote(ctx echo.Context) error {
	c := ctx.(*core.Context)

	name := path.Join(ctx.Request().URL.Host, ctx.Request().URL.Path)

	var remote models.Remote

	if err := c.DB().First(&remote, "name=?", name).Error; err != nil {
		return err
	}

	return ctx.Render(http.StatusOK, "remote.gohtml", remote)
}
