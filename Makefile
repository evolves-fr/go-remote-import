.PHONY: build
build:
	docker run --rm -it --privileged \
		-v $$PWD:/go/src/gitlab.com/evolves-fr/go-remote-import \
		-w /go/src/gitlab.com/evolves-fr/go-remote-import \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-e DOCKER_USERNAME -e DOCKER_PASSWORD -e DOCKER_REGISTRY  \
		-e GITLAB_TOKEN \
		goreleaser/goreleaser-cross release --debug --snapshot --skip-publish --clean
