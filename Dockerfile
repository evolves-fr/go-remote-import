FROM scratch
ENTRYPOINT ["/go-remote-import"]
CMD ["serve"]
COPY go-remote-import /
