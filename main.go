package main

import (
	"fmt"
	"os"

	"github.com/AlecAivazis/survey/v2"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/evolves-fr/go-remote-import/internal"
	"gitlab.com/evolves-fr/go-remote-import/internal/core"
	"gitlab.com/evolves-fr/go-remote-import/internal/models"
)

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true, ForceQuote: true})
}

func main() {
	flags := []cli.Flag{
		&cli.BoolFlag{
			Name:    "debug",
			Usage:   "Enable debug mode",
			Aliases: []string{"d"},
			EnvVars: []string{"APP_DEBUG"},
			Value:   false,
		},
		&cli.StringFlag{
			Name:    "env",
			Usage:   "Environment mode",
			EnvVars: []string{"APP_ENV"},
			Value:   "prod",
		},
		&cli.StringFlag{
			Name:    "secret",
			Usage:   "Environment mode",
			EnvVars: []string{"APP_SECRET"},
			Value:   "ChangeMe",
		},
		&cli.StringFlag{
			Name:    "database",
			Usage:   "Database DSN",
			EnvVars: []string{"APP_DATABASE"},
			Value:   "sqlite://db.sqlite",
		},
	}

	commands := []*cli.Command{
		{
			Name:   "serve",
			Usage:  "Run server",
			Action: serveAction,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:    "listen",
					Usage:   "Listen address",
					EnvVars: []string{"APP_LISTEN"},
					Value:   ":80",
				},
			},
		},
		{
			Name:   "create-user",
			Usage:  "Create a new user",
			Action: createUserAction,
		},
	}

	app := cli.App{
		Name:      "go-remote-import",
		Usage:     "Redirect Go modules with custom domain to repository",
		Copyright: "EVOLVES © 2023",
		Authors:   []*cli.Author{{Name: "Thomas FOURNIER", Email: "tfournier@evolves.fr"}},
		Version:   core.Version,
		Flags:     flags,
		Commands:  commands,
		Before:    before,
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}

func before(ctx *cli.Context) error {
	// Enable debug mode
	if ctx.Bool("debug") {
		logrus.SetLevel(logrus.DebugLevel)
	}

	return nil
}

func serveAction(ctx *cli.Context) error {
	_, _ = fmt.Fprintf(ctx.App.Writer, "Version=%s Commit=%s Date=%s\n", core.Version, core.Commit, core.Date)

	return internal.NewServer(core.Config{
		Debug:       ctx.Bool("debug"),
		Environment: ctx.String("env"),
		Secret:      ctx.String("secret"),
		Database:    ctx.String("database"),
	}, ctx.String("listen"))
}

func createUserAction(ctx *cli.Context) error {
	questions := []*survey.Question{
		{
			Name:     "login",
			Prompt:   &survey.Input{Message: "Login ?"},
			Validate: survey.Required,
		},
		{
			Name:     "password",
			Prompt:   &survey.Password{Message: "Password ?"},
			Validate: survey.Required,
		},
	}

	var user models.User

	if err := survey.Ask(questions, &user); err != nil {
		return err
	}

	c, err := core.New(core.Config{
		Debug:       ctx.Bool("debug"),
		Environment: ctx.String("env"),
		Secret:      ctx.String("secret"),
		Database:    ctx.String("database"),
	})
	if err != nil {
		return err
	}

	return c.DB().Create(&user).Error
}
